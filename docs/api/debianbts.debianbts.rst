﻿debianbts.debianbts
===================

.. automodule:: debianbts.debianbts

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_bug_log
      get_bugs
      get_soap_client_kwargs
      get_status
      get_usertag
      newest_bugs
      set_soap_location
      set_soap_proxy
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Bugreport
   
   

   
   
   



